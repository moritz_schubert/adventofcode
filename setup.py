#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  1 12:59:59 2023

@author: moritz
"""

from setuptools import find_packages, setup

setup(
    name="src",
    packages=find_packages(where="."),
)
