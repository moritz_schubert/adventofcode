#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  1 12:54:35 2023

@author: moritz
"""

# Python package index
import pytest

# custom scripts
from src import aoc_23_07 as aoc07

day = 7


@pytest.mark.parametrize(
    "hand,expected_type",
    [
        ("AAAAA", "five"),
        ("AA8AA", "four"),
        ("23332", "house"),
        ("TTT98", "three"),
        ("23432", "pair2"),
        ("A23A4", "pair1"),
        ("23456", "card"),
        ("32T3K", "pair1"),
        ("T55J5", "three"),
        ("KK677", "pair2"),
        ("KTJJT", "pair2"),
        ("QQQJA", "three"),
    ],
)
def test_hand_typing(hand, expected_type):
    hand = aoc07.Hand(hand, rule_set=1)
    assert hand.htype == expected_type


@pytest.mark.parametrize(
    "hand,expected_type",
    [
        ("32T3K", "pair1"),
        ("T55J5", "four"),
        ("KK677", "pair2"),
        ("KTJJT", "four"),
        ("QQQJA", "four"),
    ],
)
def test_hand_typing_with_jokers(hand, expected_type):
    hand = aoc07.Hand(hand, rule_set=2)
    assert hand.htype == expected_type


if __name__ == "__main__":
    pass
