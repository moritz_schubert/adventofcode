#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  1 12:54:35 2023

@author: moritz
"""

# Python package index
import pytest

# custom scripts
from src import aoc_23_09 as aoc09


@pytest.mark.parametrize(
    "history,expected_new_value",
    [
        ([0, 3, 6, 9, 12, 15], 18),
        ([1, 3, 6, 10, 15, 21], 28),
        ([10, 13, 16, 21, 30, 45], 68),
    ],
)
def test_forward_extrapolation(history, expected_new_value):
    new_value = aoc09.extrapolate_forward(history)
    assert new_value == expected_new_value


@pytest.mark.parametrize(
    "history,expected_new_value",
    [
        ([0, 3, 6, 9, 12, 15], -3),
        ([1, 3, 6, 10, 15, 21], 0),
        ([10, 13, 16, 21, 30, 45], 5),
    ],
)
def test_backward_extrapolation(history, expected_new_value):
    new_value = aoc09.extrapolate_backward(history)
    assert new_value == expected_new_value


if __name__ == "__main__":
    pass
