#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  1 12:54:35 2023

@author: moritz
"""

# Python package index
import pytest

# custom scripts
from src import utils
from src import aoc_23_01 as aoc01

day = 1


@pytest.mark.parametrize(
    "part,expected_values", [(1, [1, 3, 1, 7]), (2, [2, 8, 1, 2, 4, 1, 7])]
)
def test_get_first_digit(part, expected_values):
    fname = None if part == 1 else "2023-01_test2.txt"
    puzzle_lines = utils.get_testing_input(day, split_lines=True, fname=fname)

    for puzzle_line, expected_value in zip(puzzle_lines, expected_values):
        first_digit = aoc01.first_digit(puzzle_line, part)
        assert first_digit == expected_value, f"{puzzle_line=}"


@pytest.mark.parametrize(
    "part,expected_values", [(1, [2, 8, 5, 7]), (2, [9, 3, 3, 4, 2, 4, 6])]
)
def test_get_last_digit(part, expected_values):
    fname = None if part == 1 else "2023-01_test2.txt"
    puzzle_lines = utils.get_testing_input(day, split_lines=True, fname=fname)

    for puzzle_line, expected_value in zip(puzzle_lines, expected_values):
        last_digit = aoc01.last_digit(puzzle_line, part)
        assert last_digit == expected_value, f"{puzzle_line=}"


@pytest.mark.parametrize(
    "part,expected_values", [(1, [12, 38, 15, 77]), (2, [29, 83, 13, 24, 42, 14, 76])]
)
def test_get_calibration_value(part, expected_values):
    fname = None if part == 1 else "2023-01_test2.txt"
    puzzle_lines = utils.get_testing_input(day, split_lines=True, fname=fname)

    for puzzle_line, expected_value in zip(puzzle_lines, expected_values):
        calibration_value = aoc01.get_calibration_value(puzzle_line, part)
        assert calibration_value == expected_value


@pytest.mark.parametrize("part,expected_value", [(1, 142), (2, 281)])
def test_sum_calibration_values(part, expected_value):
    fname = None if part == 1 else "2023-01_test2.txt"
    puzzle_lines = utils.get_testing_input(day, split_lines=True, fname=fname)

    calibration_sum = aoc01.sum_calibration_values(puzzle_lines, part)

    assert expected_value == calibration_sum


if __name__ == "__main__":
    test_sum_calibration_values(1, 142)
