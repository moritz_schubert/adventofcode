#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  1 12:54:35 2023

@author: moritz
"""

# Python package index
import pytest

# custom scripts
from src import utils
from src import aoc_23_02 as aoc02

day = 1

GAME1 = "Game 1: 3 blue, 4 red; 1 red, 2 green, 6 blue; 2 green"
GAME2 = "Game 2: 1 blue, 2 green; 3 green, 4 blue, 1 red; 1 green, 1 blue"
GAME3 = "Game 3: 8 green, 6 blue, 20 red; 5 blue, 4 red, 13 green; 5 green, 1 red"
GAME4 = "Game 4: 1 green, 3 red, 6 blue; 3 green, 6 red; 3 green, 15 blue, 14 red"
GAME5 = "Game 5: 6 red, 1 blue, 3 green; 2 blue, 1 red, 2 green"


@pytest.mark.parametrize(
    "record,expected_n,expected_record",
    [
        (
            GAME1,
            1,
            [
                {"blue": 3, "red": 4, "green": 0},
                {"red": 1, "green": 2, "blue": 6},
                {"green": 2, "red": 0, "blue": 0},
            ],
        ),
        (
            GAME2,
            2,
            [
                {"blue": 1, "green": 2, "red": 0},
                {"green": 3, "blue": 4, "red": 1},
                {"green": 1, "blue": 1, "red": 0},
            ],
        ),
    ],
)
def test_clean_record(record, expected_n, expected_record):
    game_n, cleaned_record = aoc02.clean_record(record)
    assert game_n == expected_n
    assert cleaned_record == expected_record


@pytest.mark.parametrize(
    "record,expected_value",
    [
        (GAME1, {"blue": 6, "red": 4, "green": 2}),
        (GAME2, {"blue": 4, "green": 3, "red": 1}),
        (GAME3, {"blue": 6, "green": 13, "red": 20}),
        (GAME4, {"blue": 15, "green": 3, "red": 14}),
        (GAME5, {"blue": 2, "green": 3, "red": 6}),
    ],
)
def test_minimal_bag_contents(record, expected_value):
    game_n, cleaned_record = aoc02.clean_record(record)
    assert aoc02.calc_minimal_bag_contents(cleaned_record) == expected_value


CONDITION = {"blue": 14, "green": 13, "red": 12}


@pytest.mark.parametrize(
    "condition,record,expected_value",
    [
        (CONDITION, GAME1, True),
        (CONDITION, GAME2, True),
        (CONDITION, GAME3, False),
        (CONDITION, GAME4, False),
        (CONDITION, GAME5, True),
    ],
)
def test_is_game_possible(condition, record, expected_value):
    game_n, cleaned_record = aoc02.clean_record(record)
    minimal_bag_contents = aoc02.calc_minimal_bag_contents(cleaned_record)
    assert aoc02.is_game_possible(condition, minimal_bag_contents) == expected_value


def test_play_with_elf_part1():
    test_input = utils.get_testing_input(day=2)
    assert aoc02.play_with_elf_part1(test_input, CONDITION) == 8


def test_play_with_elf_part2():
    test_input = utils.get_testing_input(day=2)
    assert aoc02.play_with_elf_part2(test_input) == 2286


@pytest.mark.parametrize(
    "record,power", [(GAME1, 48), (GAME2, 12), (GAME3, 1560), (GAME4, 630), (GAME5, 36)]
)
def test_calculate_cube_power(record, power):
    game_n, cleaned_record = aoc02.clean_record(record)
    minimal_bag_contents = aoc02.calc_minimal_bag_contents(cleaned_record)
    assert aoc02.calculate_cube_power(minimal_bag_contents) == power


if __name__ == "__main__":
    test_play_with_elf(CONDITION, 8)
