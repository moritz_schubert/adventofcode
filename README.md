# Advent of Code

## Prerequisites
- Python >= 3.10
  - because of the use of | operator in type annotations ([PEP 604](https://peps.python.org/pep-0604/))

## Installation
1. clone this repository
2. in the repository's root folder, run `pip install -e .` to install the `src/` folder as a package
