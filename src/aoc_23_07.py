# -*- coding: utf-8 -*-
"""
===================
====== DAY 07 =====
--- Camel Cards ---
===================
"""

# standard library
from collections import Counter
from typing import Literal

# Python standard library
from attrs import define, field

# custom scripts
from src import utils


def create_ranking_dict(ranking: str) -> dict:
    labels = ranking.split(",")

    return {key.strip(): value for key, value in zip(labels, range(len(labels), 0, -1))}


card_labels_part1 = "A, K, Q, J, T, 9, 8, 7, 6, 5, 4, 3, 2"
card_labels_part2 = "A, K, Q, T, 9, 8, 7, 6, 5, 4, 3, 2, J"
hand_types = "five, four, house, three, pair2, pair1, card"

htype_to_value_dict = create_ranking_dict(hand_types)


@define
class Hand:
    cards: str
    rule_set: Literal[1, 2] = field(kw_only=True)  # i.e. puzzle part
    ranking: dict = field(init=False)
    values: list[int] = field(init=False)
    htype: str = field(init=False)
    htype_value: int = field(init=False)

    def __attrs_post_init__(self):
        ranking_str = card_labels_part1 if self.rule_set == 1 else card_labels_part2
        self.ranking = create_ranking_dict(ranking_str)
        self.values = [self.ranking[card] for card in list(self.cards)]
        self.htype = (
            self._determine_htype()
            if self.rule_set == 1
            else self._determine_htype_with_jokers()
        )
        self.htype_value = htype_to_value_dict[self.htype]

    def __le__(self, other):
        if self.htype_value != other.htype_value:
            return self.htype_value < other.htype_value
        elif self.htype_value == other.htype_value:
            for i in range(len(self.cards)):
                if self.values[i] != other.values[i]:
                    return self.values[i] < other.values[i]
                else:
                    continue
            # all positions have to be equal
            return True

    def __lt__(self, other):
        if self.htype_value != other.htype_value:
            return self.htype_value < other.htype_value
        elif self.htype_value == other.htype_value:
            for i in range(len(self.cards)):
                if self.values[i] != other.values[i]:
                    return self.values[i] < other.values[i]
                else:
                    continue
            # all positions have to be equal
            return False

    def __ge__(self, other):
        if self.htype_value != other.htype_value:
            return self.htype_value > other.htype_value
        elif self.htype_value == other.htype_value:
            for i in range(len(self.cards)):
                if self.values[i] != other.values[i]:
                    return self.values[i] > other.values[i]
                else:
                    continue
            # all positions have to be equal
            return True

    def __gt__(self, other):
        if self.htype_value != other.htype_value:
            return self.htype_value > other.htype_value
        elif self.htype_value == other.htype_value:
            for i in range(len(self.cards)):
                if self.values[i] != other.values[i]:
                    return self.values[i] > other.values[i]
                else:
                    continue
            # all positions have to be equal
            return False

    def _determine_htype(self):
        count_dict = Counter(self.cards)
        count_values = list(count_dict.values())
        count_values.sort(reverse=True)
        if len(count_dict) == 1:
            return "five"
        elif len(count_dict) == 2:
            if count_values[0] == 4:
                return "four"
            elif count_values[0] == 3:
                return "house"
            else:
                raise ValueError(
                    "Unexpected scenario with 2 card labels in hand",
                    count_dict,
                    f"{count_values=}",
                )
        elif len(count_dict) == 3:
            if count_values[0] == 3:
                return "three"
            elif count_values[0] == 2:
                return "pair2"
            else:
                raise ValueError(
                    "Unexpected scenario with 3 card labels in hand.",
                    count_dict,
                    f"{count_values=}",
                )
        elif len(count_dict) == 4:
            return "pair1"
        else:
            return "card"

    def _determine_htype_with_jokers(self) -> str:
        if "J" not in self.cards:
            return self._determine_htype()
        else:
            counter_dict = Counter(self.cards)
            del counter_dict["J"]
            if not counter_dict:
                return "five"
            potential_hands = []
            for card in counter_dict:
                new_cards = self.cards.replace("J", card)
                new_hand = Hand(new_cards, rule_set=1)
                potential_hands.append(new_hand)
            best_hand = max(potential_hands)

            return best_hand.htype


if __name__ == "__main__":
    puzzle_input = utils.get_puzzle_input(7)
    part = 2
    cards_bids_pairs = [line.split() for line in puzzle_input]

    hands = [Hand(cards, rule_set=part) for cards, _ in cards_bids_pairs]
    hand_to_bid_dict = {cards: int(bid) for cards, bid in cards_bids_pairs}
    total_winnings = 0

    for i, hand in enumerate(sorted(hands)):
        rank = i + 1
        bid = hand_to_bid_dict[hand.cards]
        winning = rank * bid
        total_winnings += winning

    print(total_winnings)
