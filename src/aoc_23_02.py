# -*- coding: utf-8 -*-

# standard library
import math
import re
from typing import Tuple

# custom modules
from src import utils

COLORS = ["red", "green", "blue"]


def clean_record(game_record: str) -> Tuple[int, dict]:
    re_match = re.search(r"Game (\d+):", game_record)
    n_game = re_match.groups()[0]
    n_game = int(n_game)

    game_rounds = game_record.split(":")[1].split(";")
    color_dicts = []
    for game_round in game_rounds:
        color_dict = {key: 0 for key in COLORS}
        for color in COLORS:
            re_match = re.search(f"(\\d+) {color}", game_round)
            if re_match:
                n_cubes_str = re_match.groups()[0]
                n_cubes = int(n_cubes_str)
                color_dict[color] = n_cubes
        color_dict = utils.sort_dict(color_dict)
        color_dicts.append(color_dict)

    return n_game, color_dicts


def calc_minimal_bag_contents(game_lst: list[dict]) -> dict:
    scores_by_color = {color: [] for color in COLORS}
    for round_dict in game_lst:
        for color in COLORS:
            scores_by_color[color].append(round_dict[color])

    minimal_bag_contents = {key: max(values) for key, values in scores_by_color.items()}
    minimal_bag_contents = utils.sort_dict(minimal_bag_contents)

    return minimal_bag_contents


def is_game_possible(condition: dict, minimal_bag_contents: dict) -> bool:
    possible_by_color = {
        key_cond: val_bag <= val_cond
        for (key_bag, val_bag), (key_cond, val_cond) in zip(
            minimal_bag_contents.items(), condition.items()
        )
    }
    game_is_possible = all(possible_by_color.values())

    return game_is_possible


def play_with_elf_part1(puzzle_input: list, condition: dict) -> int:
    possible_games = []
    for game_record in puzzle_input:
        game_n, cleaned_record = clean_record(game_record)
        minimal_bag_contents = calc_minimal_bag_contents(cleaned_record)
        game_possibility = is_game_possible(condition, minimal_bag_contents)
        if game_possibility:
            possible_games.append(game_n)
        else:
            continue

    return sum(possible_games)


def play_with_elf_part2(puzzle_input: list) -> int:
    powers = []
    for game_record in puzzle_input:
        _, cleaned_record = clean_record(game_record)
        minimal_bag_contents = calc_minimal_bag_contents(cleaned_record)
        cube_power = calculate_cube_power(minimal_bag_contents)
        powers.append(cube_power)

    return sum(powers)


def calculate_cube_power(minimal_bag_contents: dict) -> int:
    return math.prod(minimal_bag_contents.values())


if __name__ == "__main__":
    condition = {"blue": 14, "green": 13, "red": 12}
    puzzle_input = utils.get_puzzle_input(day=2)
    game_sum = play_with_elf_part2(puzzle_input)
