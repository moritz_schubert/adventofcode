# -*- coding: utf-8 -*-
"""
====================
====== DAY 04 ======
--- Scratchcards ---
====================
"""

# custom modules
from src import utils


def remove_index(line: str) -> str:
    return line.split(":")[1]


def split_card(line: str) -> (str, str):
    winning_numbers, my_numbers = [l.split() for l in line.split("|")]
    winning_numbers, my_numbers = [
        list(map(int, numbers)) for numbers in (winning_numbers, my_numbers)
    ]

    return winning_numbers, my_numbers


def score_points(puzzle_input: list) -> int:
    """main function for part 1"""
    points = []

    for line in puzzle_input:
        line = remove_index(line)
        winning_numbers, my_numbers = split_card(line)

        wins = get_num_of_wins(winning_numbers, my_numbers)

        if wins > 0:
            point = 2 ** (wins - 1)
            points.append(point)

    return sum(points)


def get_more_cards(puzzle_input: list) -> int:
    """main function for part 2"""
    n_lines = len(puzzle_input)
    card_collection = {i: 1 for i in range(n_lines)}

    for i_card, card in enumerate(puzzle_input):
        card = remove_index(card)
        winning_numbers, my_numbers = split_card(card)
        n_wins = get_num_of_wins(winning_numbers, my_numbers)

        num_cards = card_collection[i_card]
        for _ in range(num_cards):
            for i_win in range(1, n_wins + 1):
                card_collection[i_card + i_win] += 1

    return sum(card_collection.values())


def get_num_of_wins(winning_numbers, my_numbers) -> int:
    wins = 0

    for winning_number in winning_numbers:
        if winning_number in my_numbers:
            wins += 1

    return wins


if __name__ == "__main__":
    puzzle_input = utils.get_puzzle_input(4)

    card_sum = get_more_cards(puzzle_input)

    print(card_sum)
