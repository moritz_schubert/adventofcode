# -*- coding: utf-8 -*-
"""
=======================================
================ DAY 05 ===============
--- If You Give A Seed A Fertilizer ---
=======================================
"""

# standardlibrary
from copy import copy
import functools
import string

# Python standard library
import numpy as np
from tqdm import tqdm

# custom modules
from src import utils


def get_starting_values(puzzle_input: list) -> list[int]:
    starting_line = puzzle_input[0]
    starting_values_str = starting_line.split(":")[-1]
    starting_values_lst = from_str_to_int_list(starting_values_str)

    return starting_values_lst


def from_str_to_int_list(line: str) -> list[int]:
    line = line.strip()
    num_list = line.split(" ")
    num_list = [int(num) for num in num_list]

    return num_list


def iterate_over_conversion_maps(puzzle_input: list) -> list[list]:
    conversion_ranges = []
    map_completed = False

    for line in puzzle_input[2:]:
        if map_completed:
            conversion_ranges = []
            map_completed = False

        if line and line[0] not in string.digits:
            continue
        elif line and line[0] in string.digits:
            conversion_range = from_str_to_int_list(line)
            conversion_ranges.append(conversion_range)
        elif not line:
            map_completed = True
            yield conversion_ranges


def from_seed_to_location(starting_material: int, puzzle_input: str) -> int:
    destination_material = 0

    for i, conversion_ranges in enumerate(iterate_over_conversion_maps(puzzle_input)):
        if i == 0:
            source_material = starting_material
        else:
            source_material = destination_material

        matches_a_range = False
        for conversion_range in conversion_ranges:
            destination_start, source_start, len_range = conversion_range
            if source_start <= source_material < source_start + len_range:
                delta_source_start = source_material - source_start
                destination_material = destination_start + delta_source_start
                matches_a_range = True
        if not matches_a_range:
            destination_material = source_material

    return destination_material


def map_a_few_seeds(puzzle_input: str) -> int:
    """main function for part 1"""

    puzzle_input.append("")
    destination_materials = []

    for i, conversion_ranges in enumerate(iterate_over_conversion_maps(puzzle_input)):
        if i == 0:
            source_materials = get_starting_values(puzzle_input)
        else:
            source_materials = copy(destination_materials)
            destination_materials = []

        for source_material in source_materials:
            matches_a_range = False
            for conversion_range in conversion_ranges:
                destination_start, source_start, len_range = conversion_range
                if source_start <= source_material < source_start + len_range:
                    delta_source_start = source_material - source_start
                    destination_material = destination_start + delta_source_start
                    destination_materials.append(destination_material)
                    matches_a_range = True
            if not matches_a_range:
                destination_material = source_material
                destination_materials.append(destination_material)

    return min(destination_materials)


def map_enough_seeds(puzzle_input: str):
    """main function for part 2"""

    puzzle_input.append("")
    from_seed_to_loc = functools.partial(
        from_seed_to_location, puzzle_input=puzzle_input
    )
    from_seed_to_loc = np.vectorize(from_seed_to_loc)

    starting_values = get_starting_values(puzzle_input)
    start_end_pairs = [
        [start, start + length]
        for start, length in zip(starting_values[::2], starting_values[1::2])
    ]

    min_locations = []
    for start, end in tqdm(start_end_pairs):
        ar_range = np.arange(start, end)
        location_range = from_seed_to_loc(ar_range)
        min_location = min(location_range)
        min_locations.append(min_location)

    return min(min_locations)


if __name__ == "__main__":
    puzzle_input = utils.get_puzzle_input(5)
    puzzle_input.append("")

    result = map_enough_seeds(puzzle_input)
    print(result)
