# -*- coding: utf-8 -*-
"""
===================
====== DAY 06 =====
--- Wait For It ---
===================
"""

# standard library

# Python standard library
import numpy as np


def get_testing_input(part: int) -> list[list]:
    test_input1 = [[7, 15, 30], [9, 40, 200]]
    test_input2 = [[71530], [940200]]

    return test_input1 if part == 1 else test_input2


def get_puzzle_input(part: int) -> list[list]:
    puzzle_input1 = [[60, 80, 86, 76], [601, 1163, 1559, 1300]]
    puzzle_input2 = [[60808676], [601116315591300]]

    return puzzle_input1 if part == 1 else puzzle_input2


if __name__ == "__main__":
    # variable to switch between parts
    part = 2

    times, min_distances = get_puzzle_input(part)
    num_wins_lst = []

    for time, min_distance in zip(times, min_distances):
        distance_travelled = np.arange(time) * (time - np.arange(time))
        num_wins = sum(distance_travelled > min_distance)
        num_wins_lst.append(num_wins)

    print(np.prod(num_wins_lst))
