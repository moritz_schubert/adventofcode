# -*- coding: utf-8 -*-

# standard library
from itertools import product
import string
from typing import Tuple

# Python package index
import numpy as np

# custo scripts
from src import utils


def is_digit(char: str) -> bool:
    return char in string.digits


def is_symbol(char: str) -> bool:
    no_digit = not is_digit(char)
    no_dot = char != "."
    return no_digit and no_dot


def to_str_np_array(puzzle_input: str) -> np.ndarray:
    chars = [list(line) for line in test_input]
    schematic = np.array(chars)

    return schematic


def iterate_rows_and_cols(schematic: np.ndarray) -> Tuple[int, int]:
    n_rows, n_cols = schematic.shape
    for row in range(n_rows):
        for col in range(n_cols):
            yield row, col


def has_neighbouring_symbol(
    row: int, col: int, schematic: np.ndarray, is_part_number: bool
) -> bool:
    for neighbor_row, neighbor_col in iterate_neighbors(row, col):
        if not is_over_the_edge(neighbor_row, neighbor_col, schematic):
            neighbor_char = schematic[neighbor_row, neighbor_col]
            if is_symbol(neighbor_char):
                return True

    return False or is_part_number


def iterate_neighbors(row: int, col: int):
    for direction in product((-1, 0, 1), (-1, 0, 1)):
        if not any(direction):  # skip the position itself
            continue
        direction = np.array(direction)
        position = np.array((row, col))
        neighbor = position + direction
        neighbor_row, neighbor_col = neighbor

        yield neighbor_row, neighbor_col


def is_gear_symbol(row: int, col: int, schematic: np.ndarray):
    neighboring_numbers = 0
    for nigh_row, nigh_col in iterate_neighbors(row, col):
        if schematic[nigh_row, nigh_col] in string.digits:
            neighboring_numbers += 1

    return neighboring_numbers >= 2


def get_whole_number(
    row: int, col: int, schematic: np.ndarray, schematic_checked: np.ndarray
) -> int:
    current_number = schematic[row, col]
    assert (
        current_number in string.digits
    ), "This function should not be called if row, col do not point to a number."

    for direction in (-1, 1):
        next_col = col
        current_char = schematic[row, col]
        while is_digit(current_char):
            next_col += direction
            if is_over_the_edge(row, next_col, schematic):
                break
            current_char = schematic[row, next_col]
            has_been_checked = schematic_checked[row, next_col]
            if not is_digit(current_char) or has_been_checked:
                break
            else:
                if direction == -1:
                    current_number = current_char + current_number
                else:
                    current_number += current_char

                schematic_checked[row, next_col] = True

    current_number = int(current_number)

    return current_number, schematic_checked


def get_neighboring_numbers(
    row: int, col: int, schematic: np.ndarray, schematic_checked: np.ndarray
) -> (list[int, int], np.ndarray):
    neighboring_numbers = []

    for nigh_row, nigh_col in iterate_neighbors(row, col):
        has_been_checked = schematic_checked[nigh_row, nigh_col]
        if has_been_checked:
            continue
        nigh_char = schematic[nigh_row, nigh_col]
        schematic_checked[nigh_row, nigh_col] = True
        if nigh_char in string.digits:
            neighboring_number, schematic_checked = get_whole_number(
                nigh_row, nigh_col, schematic, schematic_checked
            )
            neighboring_numbers.append(neighboring_number)

    return neighboring_numbers, schematic_checked


def is_over_the_edge(row: int, col: int, schematic: np.ndarray) -> bool:
    n_rows, n_cols = schematic.shape
    too_low = row < 0 or col < 0
    too_high = row >= n_rows or col >= n_cols
    return too_low or too_high


def find_missing_part(puzzle_input: list) -> int:
    """main function of part 1"""
    schematic = to_str_np_array(test_input)

    part_numbers = []
    current_number = ""
    last_char = ""
    is_part_number = False

    for row, col in iterate_rows_and_cols(schematic):
        current_char = schematic[row, col]
        if current_char in string.digits:
            current_number += current_char
            is_part_number = has_neighbouring_symbol(
                row, col, schematic, is_part_number
            )
        if last_char in string.digits and current_char not in string.digits:
            if is_part_number:
                part_numbers += [int(current_number)]
            is_part_number = False
            current_number = ""

        last_char = current_char

    return sum(part_numbers)


if __name__ == "__main__":
    test_input = utils.get_puzzle_input(3)

    # sum_part_numbers = find_missing_part(test_input) # 4361

    schematic = to_str_np_array(test_input)
    schematic_checked = np.zeros_like(schematic)

    gear_ratios = []

    for row, col in iterate_rows_and_cols(schematic):
        current_char = schematic[row, col]
        if current_char == "*":
            neighboring_numbers, schematic_checked = get_neighboring_numbers(
                row, col, schematic, schematic_checked
            )
            if len(neighboring_numbers) <= 1:
                continue
            gear_num1, gear_num2 = neighboring_numbers
            gear_ratio = gear_num1 * gear_num2
            gear_ratios.append(gear_ratio)

    print(sum(gear_ratios))
