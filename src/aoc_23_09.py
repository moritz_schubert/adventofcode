# -*- coding: utf-8 -*-
"""
==========================
========= DAY 09 =========
--- Mirage Maintenance ---
==========================
"""

# standard library

# Python package index
import numpy as np

# custom scripts
from src import utils


def extrapolate_forward(num_sequence: list[int]) -> int:
    sum_last_nums = 0
    while np.any(num_sequence):
        sum_last_nums += num_sequence[-1]
        num_sequence = np.diff(num_sequence)

    return sum_last_nums


def extrapolate_backward(num_sequence: list[int]) -> int:
    first_nums = []
    while np.any(num_sequence):
        first_nums.append(num_sequence[0])
        num_sequence = np.diff(num_sequence)

    day_minus1 = 0
    for first_num in reversed(first_nums):
        day_minus1 = first_num - day_minus1

    return day_minus1


def extrapolate_values(puzzle_input: list[str], part: int) -> int:
    """main function"""

    histories = read_in_histories(puzzle_input)
    extrapolate_func = extrapolate_forward if part == 1 else extrapolate_backward

    total_sum = 0
    for history in histories:
        total_sum += extrapolate_func(history)

    return total_sum


def read_in_histories(puzzle_input: list[str]) -> list[list[int]]:
    list_of_lists = []

    for line in puzzle_input:
        list_of_nums = [int(num) for num in line.split()]
        list_of_lists.append(list_of_nums)

    return list_of_lists


if __name__ == "__main__":
    puzzle_input = utils.get_puzzle_input(9)

    result = extrapolate_values(puzzle_input, part=2)

    print(result)
