#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# standard library
from typing import Literal

# custom modules
from src import utils

SPELLED_OUT_DIGITS = [
    "one",
    "two",
    "three",
    "four",
    "five",
    "six",
    "seven",
    "eight",
    "nine",
]


def first_numeric_digit(string: str) -> int:
    """return the first digit of the string"""
    for c in string:
        if c.isnumeric():
            return int(c)
        else:
            continue


def first_alphanumeric_digit(string: str) -> int:
    """return the first numeric or spelled out digit of the string"""
    for i_string in range(len(string)):
        current_character = string[i_string]
        for i_digit, digit in enumerate(SPELLED_OUT_DIGITS):
            if digit == string[i_string : i_string + len(digit)]:
                return i_digit + 1
            elif current_character.isnumeric():
                return int(current_character)


def last_numeric_digit(string: str) -> int:
    """return the last digit of the string"""

    for c in reversed(string):
        if c.isnumeric():
            return int(c)
        else:
            continue


def last_alphanumeric_digit(string: str) -> int:
    """return the last numeric or spelled out digit of the string"""
    for i_string in range(len(string)):
        current_character = string[-(i_string + 1)]
        for i_digit, digit in enumerate(SPELLED_OUT_DIGITS):
            if (
                digit
                == string[len(string) - len(digit) - i_string : len(string) - i_string]
            ):
                return i_digit + 1
            elif current_character.isnumeric():
                return int(current_character)


def first_digit(string: str, part: Literal[1, 2]) -> int:
    if part == 1:
        return first_numeric_digit(string)
    else:
        return first_alphanumeric_digit(string)


def last_digit(string: str, part: Literal[1, 2]) -> int:
    if part == 1:
        return last_numeric_digit(string)
    else:
        return last_alphanumeric_digit(string)


def get_calibration_value(line: str, part: Literal[1, 2]) -> int:
    """get the first and last digit of the string and concatenate them"""

    first = first_digit(line, part)
    last = last_digit(line, part)
    calibration_value = int(str(first) + str(last))

    return calibration_value


def sum_calibration_values(lines: list, part: Literal[1, 2]) -> int:
    """extract calibration values and return their sum"""

    calibration_values = []
    for line in lines:
        calibration_value = get_calibration_value(line, part)
        calibration_values.append(calibration_value)

    return sum(calibration_values)


def recover_calibration_values(part: Literal[1, 2]) -> int:
    """recover the calibration values sum from the 'creatively augmented' input

    main function for round 1 & 2
    """

    input_lines = utils.get_puzzle_input(day=1)

    calibration_sum = sum_calibration_values(input_lines, part=part)

    return calibration_sum


if __name__ == "__main__":
    calibration_sum = recover_calibration_values(2)
    print(calibration_sum)
