# -*- coding: utf-8 -*-
"""
=========================
========= DAY 08 ========
--- Haunted Wasteland ---
=========================
"""

# standard library
import itertools

# Python standard library

# custom scripts
from src import utils


def get_directions(puzzle_input: list) -> str:
    return puzzle_input[0]


def read_in_desert_map(puzzle_input: list) -> dict:
    desert_map = {}

    for line in puzzle_input[2:]:
        location, crossroad_str = [element.strip() for element in line.split("=")]
        crossroad_str = crossroad_str.strip("()")
        left, right = [element.strip() for element in crossroad_str.split(",")]
        desert_map[location] = (left, right)

    return desert_map


def navigate_desert_humanly(puzzle_input: list) -> int:
    """main function for part 1"""

    directions = get_directions(puzzle_input)
    direction_to_index = {"L": 0, "R": 1}

    desert_map = read_in_desert_map(puzzle_input)
    current_location = "AAA"
    steps = 0

    for direction in itertools.cycle(directions):
        current_crossroad = desert_map[current_location]
        crossroad_i = direction_to_index[direction]
        current_location = current_crossroad[crossroad_i]
        steps += 1
        if current_location == "ZZZ":
            break

    return steps


if __name__ == "__main__":
    puzzle_input = utils.get_puzzle_input(8)

    directions = get_directions(puzzle_input)
    direction_to_index = {"L": 0, "R": 1}

    desert_map = read_in_desert_map(puzzle_input)
    current_locations = []
    for location in desert_map:
        if location[-1] == "A":
            current_locations.append(location)
    steps = 0

    for direction in itertools.cycle(directions):
        for loc_i, location in enumerate(current_locations):
            current_crossroad = desert_map[current_locations[loc_i]]
            road_i = direction_to_index[direction]
            current_locations[loc_i] = current_crossroad[road_i]
        steps += 1
        if steps % 100_000 == 0:
            print(steps)
        reached_z = [current_loc[-1] == "Z" for current_loc in current_locations]
        if all(reached_z):
            break

    print(steps)
