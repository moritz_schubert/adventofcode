#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Dec  1 12:56:23 2023

@author: moritz
"""
# standard library
from enum import Enum
from pathlib import Path

ROOT_PATH = Path(__file__).parent.parent
current_year = 2023


def get_fname(year: int, day: int, *, testing: bool = False) -> str:
    day_str = str(day).zfill(2)
    file_type = "test" if testing else "puzzle"
    fname = f"{year}-{day_str}_{file_type}.txt"

    return fname


def _get_input(
    day: int,
    year: int = current_year,
    *,
    fname: str | None = None,
    split_lines: bool = True,
    testing: bool = False,
) -> str | list:
    if not fname:
        fname = get_fname(year, day, testing=testing)
    day_str = str(day).zfill(2)
    input_path = ROOT_PATH / "data" / str(year) / day_str / fname
    with open(input_path) as file:
        if split_lines == True:
            lines = file.readlines()
            puzzle_input = [line.strip() for line in lines]
        else:
            puzzle_input = file.read()

    return puzzle_input


def get_testing_input(
    day: int,
    *,
    split_lines: bool = True,
    year: int = current_year,
    fname: str | None = None,
) -> str | list:
    return _get_input(day, testing=True, fname=fname)


def get_puzzle_input(
    day: int,
    *,
    split_lines: bool = True,
    year: int = current_year,
    fname: str | None = None,
) -> str | list:
    return _get_input(day, testing=False, fname=fname)


def sort_dict(dictionary: dict) -> dict:
    return dict(sorted(dictionary.items()))


class CustomEnum(Enum):
    """custom enum class to get the name of the enum as lowercase."""

    def __init__(self, name):  # pylint: disable=unused-argument
        self._name_ = self.name.lower()

    @classmethod
    def items(cls):
        items = {key.lower(): value.value for key, value in cls.__members__.items()}

        return items

    @classmethod
    def names(cls):
        cls_keys = [member.lower() for member in cls.__members__]

        return cls_keys


if __name__ == "__main__":
    my_input = get_puzzle_input(1)
